// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var minifyCSS = require('gulp-minify-css');
var inject = require('gulp-inject');
var clean = require('gulp-clean');
var runSequence = require('run-sequence');
var jscs = require('gulp-jscs');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var exec = require('child_process').exec;

// Run gulp
gulp.task('run',['sass', 'sass:watch','build-index'], function (cb) {
  exec('lite-server', function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});
 
// Convert from scss to css
gulp.task('sass', function () {
  return gulp.src('app/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});
 
// Watch for scss changes 
gulp.task('sass:watch', function () {
  gulp.watch('app/**/*.scss', ['sass']);
});

// Check code syntax
gulp.task('review', function() {
  return gulp.src('app/**/*.js')
    .pipe(jscs())
    .pipe(jscs.reporter());
});

// Lint Task
gulp.task('lint', function() {
    return gulp.src('app/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Concatenate & Minify JS
gulp.task('minify-scripts', function() {
    return gulp.src(['./app/core/core.module.js',
                    './app/**/!(*app).module.js',
                    './app/app.module.js',
                    './app/**/*.js', '!app/**/*.test.js'])
        .pipe(concat('all.js'))
        .pipe(gulp.dest('dist'))
        .pipe(rename('all.min.js'))
        .pipe(uglify().on('error', gutil.log))
        .pipe(gulp.dest('dist/js'));
});

// Move node_modules
gulp.task('move-node-modules', function() {
  gulp.src(['./node_modules/angular/angular.js',
            './node_modules/angular-ui-router/release/angular-ui-router.js',
            './node_modules/angular-ui-bootstrap/dist/ui-bootstrap.js',
            './node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js'])
    .pipe(gulp.dest('dist/vendor/js'))

  gulp.src('./node_modules/bootstrap/fonts/*.*')
    .pipe(gulp.dest('dist/vendor/fonts'));

  return gulp.src(['node_modules/bootstrap/dist/css/bootstrap.min.css',
                   'node_modules/ng-notify/dist/ng-notify.min.css'])
          .pipe(gulp.dest('dist/vendor/css'));

});

// CSS
gulp.task('minify-styles', function() {
    return gulp.src('./css/style.css')
        .pipe(minifyCSS({comments:true, spare:true}))
        .pipe(gulp.dest('dist/css'));
});

// Fill index
gulp.task('build-index', function() {
  var target = gulp.src('./index.html');
  var sources = gulp.src(['./node_modules/angular/angular.js',
                          './node_modules/angular-ui-router/release/angular-ui-router.js',
                          './node_modules/angular-ui-bootstrap/dist/ui-bootstrap.js',
                          './node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
                          './node_modules/ngstorage/ngStorage.js',
                          './app/app.module.js',
                          './app/core/core.module.js',
                          './app/**/*.module.js',
                          './app/**/*.js',
                          'node_modules/bootstrap/dist/css/bootstrap.min.css',
                          'node_modules/ng-notify/dist/ng-notify.min.js',
                          'node_modules/ng-notify/dist/ng-notify.min.css',
                          './css/app.css',
                          './css/style.css'], {read: false});

  return target.pipe(inject(sources, {relative: true}))
      .pipe(gulp.dest('./'));

});

// Move Images
gulp.task('move-images', function() {
    return gulp.src('./img/**/*')
        .pipe(gulp.dest('dist/assets/img'));
});

// Move Index
gulp.task('move-index', function() {
    return gulp.src('./index.html')
        .pipe(gulp.dest('dist'));
});

// Move html
gulp.task('move-html', function() {
    return gulp.src('./app/**/*.html')
        .pipe(gulp.dest('dist/app'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('app/**/*.js', ['lint', 'review', 'build-index']);
    gulp.watch('assets/stylesheets/**/*.css', ['build-index']);
});

// Remove build folder
gulp.task('clean', function() {
    gulp.src('./dist/*')
        .pipe(clean({force: true}));
});

// Default Task
gulp.task('default', ['lint', 'review', 'build-index', 'watch']);

gulp.task('build', function() {
  runSequence(
    ['clean'],
    ['lint', 'review', 'build-index']
  );
});

gulp.task('build-production', ['move-index', 'move-node-modules',
          'minify-styles', 'minify-scripts', 'move-images', 'move-html'], function() {

    var target = gulp.src('./dist/index.html');
    var sources = gulp.src(['./vendor/js/angular.js',
                            './vendor/js/ui-bootstrap.js',
                            './vendor/js/**/*.js',
                            './vendor/css/**/*.css',
                            './all.js',
                            './css/**/*.css'], {read: false, cwd: __dirname + '/dist'});

    return target.pipe(inject(sources))
        .pipe(gulp.dest('./dist'));

});
