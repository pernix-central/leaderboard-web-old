(function() {
  'use strict';

  angular
    .module('leaderboard', [
      'leaderboard.core',
      'leaderboard.main',
      'leaderboard.activities',
      'leaderboard.presentation'
    ]);
})();
