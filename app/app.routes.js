(function() {
  'use strict';

  angular
    .module('leaderboard')
    .config(function($urlRouterProvider,
                     $stateProvider) {
      $stateProvider
        .state('header', {
          abstract: true,
          templateUrl: 'app/layout/header.html',
          controller: 'HeaderCtrl as vm'
        });
      $urlRouterProvider.otherwise('/main');
    });
})();
