(function() {
  'use strict';

  angular
    .module('leaderboard.core', [
      'ui.router',
      'ui.bootstrap',
      'ngStorage',
      'ngNotify'
    ]);
})();
