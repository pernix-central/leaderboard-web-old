(function() {
  'use strict';

  /* @ngInject */
  angular
    .module('leaderboard.core')
    .constant('HTTP_SETTINGS', {
      BASE_URL: 'http://localhost:49782/api/',
      DELETE: 'DELETE',
      GET: 'GET',
      POST: 'POST',
      PUT: 'PUT'
    });
})();
