(function() {
  'use strict';

  angular
    .module('leaderboard.core')
    .service('imageLoadingService', imageLoadingService);

  /* @ngInject */
  function imageLoadingService() {
    this.checkImage = checkImage;
  }

  function checkImage(photoUrl,
                      fallbackImg,
                      callback) {
    var image = new Image();
    image.src = photoUrl;
    image.onerror = function() {
      callback(fallbackImg);
    };
    image.onload = function() {
      callback(photoUrl);
    };
  }
})();
