(function() {
  'use strict';

  angular
    .module('leaderboard.core')
    .service('addPointsManageService', addPointsManageService);

  /* @ngInject */
  function addPointsManageService($http,
                                  $log,
                                  HTTP_SETTINGS) {
    this.addPoints = addPoints;

    function addPoints(participation) {
      var request = {
        method: HTTP_SETTINGS.POST,
        url: HTTP_SETTINGS.BASE_URL +
        'addpoints/addPoints',
        headers: {
          'Content-Type': 'application/json'
        },
        data: participation
      };
      return $http(request);
    }
  }
})();
