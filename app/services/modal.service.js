(function() {
  'use strict';

  angular
    .module('leaderboard.core')
    .service('modalService', modalService);

  /* @ngInject */
  function modalService($uibModal) {
    this.showModal = showModal;
    this.showModalWithParam = showModalWithParam;

    function showModal(templateUrl,
                       controllerName,
                       modalSize) {
      controllerName += ' as vm';
      var modal = $uibModal.open({
        templateUrl: templateUrl,
        controller: controllerName,
        windowClass: 'right fade',
        size: modalSize
      });
      return modal;
    }

    function showModalWithParam(templateUrl,
                                controllerName,
                                modalSize,
                                param) {
      controllerName += ' as vm';
      var modal = $uibModal.open({
        templateUrl: templateUrl,
        controller: controllerName,
        windowClass: 'right fade',
        size: modalSize,
        resolve: {
          param: function() {
            return param;
          }
        }
      });
      return modal;
    }
  }
})();
