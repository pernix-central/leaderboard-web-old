(function() {
  'use strict';

  angular
    .module('leaderboard.core')
    .service('activityManageService', activityManageService);

  /* @ngInject */
  function activityManageService($http,
                                 $log,
                                 HTTP_SETTINGS) {
    this.getActivities = getActivities;
    this.deleteActivity = deleteActivity;
    this.getFrequencies = getFrequencies;
    this.addActivity = addActivity;
    this.editActivity = editActivity;

    function getActivities() {
      var request = {
        method: HTTP_SETTINGS.GET,
        url: HTTP_SETTINGS.BASE_URL + 'activities/getActivities',
        headers: {
          'Content-Type': 'application/json'
        }
      };
      return $http(request);
    }

    function deleteActivity(activity) {
      var request = {
        method: HTTP_SETTINGS.DELETE,
        url: HTTP_SETTINGS.BASE_URL +
        'activities/deleteActivity?id=' + activity.idActivity,
        headers: {
          'Content-Type': 'application/json'
        }
      };
      return $http(request);
    }

    function getFrequencies() {
      var request = {
        method: HTTP_SETTINGS.GET,
        url: HTTP_SETTINGS.BASE_URL +
        'activities/getFrequencies',
        headers: {
          'Content-Type': 'application/json'
        }
      };
      return $http(request);
    }

    function addActivity(activity) {
      var request = {
        method: HTTP_SETTINGS.POST,
        url: HTTP_SETTINGS.BASE_URL +
        'activities/addActivity',
        headers: {
          'Content-Type': 'application/json'
        },
        data: activity
      };
      return $http(request);
    }

    function editActivity(activity) {
      var request = {
        method: HTTP_SETTINGS.PUT,
        url: HTTP_SETTINGS.BASE_URL +
        'activities/editActivity',
        headers: {
          'Content-Type': 'application/json'
        },
        data: activity
      };
      return $http(request);
    }
  }
})();
