(function() {
  'use strict';

  angular
    .module('leaderboard.core')
    .service('employeeManageService', employeeManageService);

  /* @ngInject */
  function employeeManageService($http,
                                 $log,
                                 HTTP_SETTINGS) {
    this.getEmployeeList = getEmployeeList;
    this.deleteMember = deleteMember;
    this.addMember = addMember;
    this.editMember = editMember;

    function getEmployeeList() {
      var request = {
        method: HTTP_SETTINGS.GET,
        url: HTTP_SETTINGS.BASE_URL + 'employees/getEmployees',
        headers: {
          'Content-Type': 'application/json'
        }
      };

      return $http(request);
    }

    function deleteMember(member) {
      var request = {
        method: HTTP_SETTINGS.DELETE,
        url: HTTP_SETTINGS.BASE_URL + 'employees/deleteMember?id=' + member.id,
        headers: {
          'Content-Type': 'application/json'
        }
      };
      return $http(request);
    }

    function addMember(member) {
      var request = {
        method: HTTP_SETTINGS.POST,
        url: HTTP_SETTINGS.BASE_URL + 'employees/addMember',
        headers: {
          'Content-Type': 'application/json'
        },
        data: member
      };
      return $http(request);
    }

    function editMember(member) {
      var request = {
        method: HTTP_SETTINGS.PUT,
        url: HTTP_SETTINGS.BASE_URL + 'employees/editMember',
        headers: {
          'Content-Type': 'application/json'
        },
        data: member
      };
      return $http(request);
    }
  }
})();
