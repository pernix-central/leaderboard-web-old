(function() {
  'use strict';

  angular
    .module('leaderboard.core')
    .service('roleManageService', roleManageService);

  /* @ngInject */
  function roleManageService($http,
                             $log,
                             HTTP_SETTINGS) {
    this.getRoles = getRoles;

    function getRoles() {
      var request = {
        method: HTTP_SETTINGS.GET,
        url: HTTP_SETTINGS.BASE_URL +
        'role/getRoles',
        headers: {
          'Content-Type': 'application/json'
        }
      };
      return $http(request);
    }
  }
})();
