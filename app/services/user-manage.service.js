(function() {
  'use strict';

  angular
    .module('leaderboard.core')
    .service('userManageService', userManageService);

  /* @ngInject */
  function userManageService($http,
                             $log,
                             HTTP_SETTINGS) {
    this.addUser = addUser;

    function addUser(user) {
      var request = {
        method: HTTP_SETTINGS.POST,
        url: HTTP_SETTINGS.BASE_URL +
        'user/addUser',
        headers: {
          'Content-Type': 'application/json'
        },
        data: user
      };
      return $http(request);
    }
  }
})();
