(function() {
  'use strict';

  angular
    .module('leaderboard.core')
    .service('jobManageService', jobManageService);

  /* @ngInject */
  function jobManageService($http,
                            $log,
                            HTTP_SETTINGS) {
    this.getJobPositions = getJobPositions;

    function getJobPositions() {
      var request = {
        method: HTTP_SETTINGS.GET,
        url: HTTP_SETTINGS.BASE_URL +
        'job/getJobPositions',
        headers: {
          'Content-Type': 'application/json'
        }
      };
      return $http(request);
    }
  }
})();
