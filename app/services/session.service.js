(function() {
  'use strict';

  angular
    .module('leaderboard.core')
    .service('sessionService', sessionService);

  /* @ngInject */
  function sessionService($q,
                          $http,
                          $localStorage,
                          HTTP_SETTINGS) {

    var sessionActive = false;
    var deferred = $q.defer();

    this.loginSession = loginSession;
    this.logoutSession = logoutSession;
    this.getSessionActive = getSessionActive;
    this.validateToken = validateToken;

    function loginSession(username, password) {
      deferred = $q.defer();
      var request = {
        method: 'POST',
        url: HTTP_SETTINGS.BASE_URL + 'session/login',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'username': username,
          'password': password,
          'token': $localStorage.token
        }
      };

      $http(request)
        .then(handleSuccess)
        .catch(handleError);
      return deferred.promise;
    }

    function logoutSession() {
      deferred = $q.defer();
      sessionActive = false;
      $localStorage.$reset();
      deferred.resolve(sessionActive);
      return deferred.promise;
    }

    function validateToken() {
      deferred = $q.defer();
      var request = {
        method: 'POST',
        url: HTTP_SETTINGS.BASE_URL + 'session/authorization',
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'code': $localStorage.token
        }
      };

      $http(request)
        .then(handleSuccess)
        .catch(handleError);
      return deferred.promise;
    }

    function handleSuccess(response) {
      sessionActive = true;
      $localStorage.token = response.data;
      deferred.resolve(sessionActive);
    }

    function handleError(response) {
      var messageError = response.data;
      if (messageError === 'Token invalid' ||
      messageError === 'Token expired') {
        $localStorage.$reset();
      }
      deferred.reject(sessionActive);
    }

    function getSessionActive() {
      return sessionActive;
    }

  }
})();
