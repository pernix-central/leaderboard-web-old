(function() {
  'use strict';

  angular
    .module('leaderboard.presentation', ['leaderboard.core'])
    .config(routesConfiguration);

  /* @ngInject */
  function routesConfiguration($stateProvider) {
    $stateProvider
      .state('header.presentation', {
        cache: false,
        url: '/presentation',
        templateUrl: 'app/presentation/presentation.html',
        controller: 'PresentationCtrl as vm'
      });
  }
})();
