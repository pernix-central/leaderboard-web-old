(function() {
  'use.strict';

  angular
    .module('leaderboard.presentation')
    .controller('PresentationCtrl', PresentationCtrl);

  /* @ngInject */
  function PresentationCtrl(employeeManageService) {
    var vm = this;
    loadData();

    function loadData() {
      employeeManageService.getEmployeeList().then(function(employeeList) {
        vm.employees = employeeList.data;
      });
    }
  }
})();
