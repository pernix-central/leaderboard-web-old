(function() {
  'use strict';
  var baseUrl = 'https://s3.amazonaws.com/pernix-site/images/members/';
  /* @ngInject */
  angular
    .module('leaderboard.presentation')
    .constant('PRESENTATION_TEMP_EMPLOYEES', [
      {
        name: 'Jose Pablo Flores',
        surname: 'jflores',
        pts: 50,
        imageUrl: baseUrl + 'jose_pablo.jpg',
        position: '1'
      },
      {
        name: 'Jesus Ramos',
        surname: 'jramos',
        pts: 30,
        imageUrl: baseUrl + 'jesus.jpg',
        position: '2'},
      {
        name: 'Saul Zamora',
        surname: 'szamora',
        pts: 22,
        imageUrl: baseUrl + 'saul.jpg',
        position: '3'},
      {
        name: 'Fernando Cardoce',
        surname: 'fcardoce',
        pts: 17,
        imageUrl: baseUrl + 'cardoce.jpg',
        position: '4'},
      {
        name: 'Emmanuel Murillo',
        surname: 'emurillo',
        pts: 12,
        imageUrl: baseUrl + 'emmanuel.jpg',
        position: '5'},
      {
        name: 'Matthew Bertelsen',
        surname: 'mbertelsen',
        pts: 10,
        imageUrl: baseUrl + 'matthew.jpg',
        position: '6'},
      {
        name: 'Carlos Sirias',
        surname: 'csirias',
        pts: 9,
        imageUrl: baseUrl + 'carlos.jpg',
        position: '7'},
      {
        name: 'Brandon Chaves',
        surname: 'bchaves',
        pts: 6,
        imageUrl: baseUrl + 'brandon_chaves.jpg',
        position: '8'},
      {
        name: 'Ricardo Franco',
        surname: 'rchaves',
        pts: 6,
        imageUrl: baseUrl + 'ricardo.jpg',
        position: '9'},
      {
        name: 'Kevin Escobar',
        surname: 'kescobar',
        pts: 4,
        imageUrl: baseUrl + 'kevin_escobar.jpg',
        position: '10'},
      {
        name: 'Gloriana Odomeo',
        surname: 'godomeo',
        pts: 2,
        imageUrl: baseUrl + 'gloriana_omodeo.jpg',
        position: '11'
      },
      {
        name: 'Carlos Chacon',
        surname: 'cchacon',
        pts: 1,
        imageUrl: baseUrl + 'chacon.jpg',
        position: '12'
      }
    ]);
})();
