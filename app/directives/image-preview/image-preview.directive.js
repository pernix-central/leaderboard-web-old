(function() {
  'use.strict';

  angular
    .module('leaderboard.main')
    .directive('leaderboardImagePreview', leaderboardImagePreview);

  function leaderboardImagePreview(imageLoadingService) {
    var fallbackImg = 'img/img-error.png';
    var directive = {
      restrict: 'A',
      link: function(scope, element, attrs) {
        scope.$watch('vm.photoUrl', function(currentPhoto) {
          imageLoadingService
          .checkImage(currentPhoto, fallbackImg, function(resultImage) {
            element.css({'background-image': 'url(' + resultImage + ')'});
          });
        });
      }
    };
    return directive;
  }
})();
