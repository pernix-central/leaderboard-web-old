(function() {
  'use.strict';

  angular
    .module('leaderboard.activities')
    .directive('activityDetail', ['$compile',
                                  activityDetail]);

  function activityDetail($compile) {
    var directive = {
      restrict: 'A',
      link: function(scope, element, attrs) {
        var dataBind = {};
        element.bind('click', function() {
          changeState(element, attrs);
          var currentDetails = JSON.parse(attrs.data);
          if (attrs.clicked == 'true') {
            dataBind.activityid = attrs.activityid;
            dataBind.detail = currentDetails.description;
            dataBind.inCharge = currentDetails.inCharge;
            dataBind.style = isEven(attrs.rowIndex) ?
                             'gray-row' : 'white-row';
            detailsRow = getTemplate(dataBind);
            var el = angular.element(detailsRow);
            $compile(el)(scope);
            element.parent().after(el);
          } else {
            var colorRow = 'color' + attrs.activityid;
            var detailRow = 'detail' + attrs.activityid;
            document.getElementById(colorRow).remove();
            document.getElementById(detailRow).remove();
          }
        });
      }
    };
    return directive;
  }

  function isEven(number) {
    return number % 2 == 0;
  }

  function getTemplate(dataBind) {
    var detailTemplate =
    '<tr id ="color' + dataBind.activityid +
    '"><tr class="table-row" id = "detail' + dataBind.activityid + '">' +
    '<td colspan=5 ' + 'align="left" class="detail-text bindStyle">' +
    '<p><i>bindDetail</i></p><p><i> ' +
    'In Charge: bindInCharge</i></p></td></tr></tr>';
    detailTemplate = detailTemplate.replace(/bindStyle/g, dataBind.style);
    detailTemplate = detailTemplate.replace(/bindDetail/g, dataBind.detail);
    detailTemplate = detailTemplate.replace(/bindInCharge/g, dataBind.inCharge);
    return detailTemplate;
  }

  function changeState(element, attrs) {
    if (attrs.clicked == 'false') {
      attrs.clicked = 'true';
      element.children()
      .removeClass('glyphicon-menu-down btn-detail')
      .addClass('glyphicon-menu-up active-detail');
    } else {
      attrs.clicked = 'false';
      element.children()
      .removeClass('glyphicon-menu-up active-detail')
      .addClass('glyphicon-menu-down btn-detail');
    }
  }
})();
