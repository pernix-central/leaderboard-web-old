(function() {
  'use.strict';

  angular
    .module('leaderboard.core')
    .directive('closeModal', closeModal);

  function closeModal($uibModalStack) {

    var directive = {
      restrict: 'A',
      link: function(scope, element, attrs) {
        element.bind('click', function() {
          $uibModalStack.dismissAll('closing');
        });
      }
    };

    return directive;
  }

})();
