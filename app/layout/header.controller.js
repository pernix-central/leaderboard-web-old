(function() {
  'use.strict';

  angular
    .module('leaderboard')
    .controller('HeaderCtrl', HeaderCtrl);

  /* @ngInject */
  function HeaderCtrl($state,
                      modalService,
                      sessionService) {

    var vm = this;
    vm.goToPresentationMode = goToPresentationMode;
    vm.goToMainView = goToMainView;
    vm.goToActivityList = goToActivityList;
    vm.getSessionActive = getSessionActive;
    vm.showLoginModal = showLoginModal;
    vm.showLogoutModal = showLogoutModal;

    function goToPresentationMode() {
      $state.go('header.presentation');
    }

    function goToMainView() {
      $state.go('header.main');
    }

    function getSessionActive() {
      return sessionService.getSessionActive();
    }

    function showLoginModal() {
      var loginTemplate = 'app/main/modals/login/login.html';
      modalService.showModal(loginTemplate, 'LoginCtrl', 'md');
    }

    function showLogoutModal() {
      var logoutTemplate = 'app/main/modals/logout/logout.html';
      modalService.showModal(logoutTemplate, 'LogoutCtrl', 'md');
    }

    function goToActivityList() {
      $state.go('header.activities');
    }
  }
})();
