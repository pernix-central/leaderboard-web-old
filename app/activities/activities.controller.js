(function() {
  'use.strict';

  angular
    .module('leaderboard.activities')
    .controller('ActivitiesCtrl', ActivitiesCtrl);

  /* @ngInject */
  function ActivitiesCtrl(modalService,
                          sessionService,
                          activityManageService) {
    var vm = this;
    vm.sessionActive = sessionService.getSessionActive;
    vm.deleteActivity = deleteActivity;
    vm.editActivity = editActivity;
    loadActivities();

    function loadActivities() {
      vm.activities = [];
      activityManageService.getActivities()
      .then(function(activityList) {
        vm.activities = activityList.data;
        vm.rowState = Array(vm.activities.length).fill(true);
      });
    }

    function deleteActivity(activity) {
      var deleteMemberTemplate =
      'app/activities/modals/delete-activity/delete-activity.html';
      modalService.showModalWithParam(deleteMemberTemplate,
        'DeleteActivityCtrl',
        'md',
        activity)
      .closed
      .then(loadActivities);
    }

    function editActivity(activity) {
      var editActivityTemplate =
      'app/activities/modals/edit-activity/edit-activity.html';
      modalService.showModalWithParam(editActivityTemplate,
        'EditActivityCtrl',
        'lg',
        activity)
      .closed
      .then(loadActivities);
    }
  }
})();
