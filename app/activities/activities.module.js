(function() {
  'use strict';

  angular
    .module('leaderboard.activities', ['leaderboard.core'])
    .config(routesConfiguration);

  /* @ngInject */
  function routesConfiguration($stateProvider) {
    $stateProvider
      .state('header.activities', {
        cache: false,
        url: '/activities',
        templateUrl: 'app/activities/activities.html',
        controller: 'ActivitiesCtrl as vm'
      });
  }
})();
