(function() {
  'use.strict';

  angular
    .module('leaderboard.activities')
    .controller('EditActivityCtrl', EditActivityCtrl);

  /* @ngInject */
  function EditActivityCtrl($q,
                           $uibModalInstance,
                           param,
                           activityManageService,
                           employeeManageService,
                           ngNotify) {
    var vm = this;
    var currentActivity = param;
    vm.substractPoints = substractPoints;
    vm.addPoints = addPoints;
    vm.editActivity = editActivity;
    activate();

    function activate() {
      getFrequencies();
      getMembers();
      fillForm();
    }

    function fillForm() {
      vm.points = 0;
      vm.activityName = currentActivity.name;
      vm.startDate = currentActivity.startDate;
      vm.points = currentActivity.points;
      vm.description = currentActivity.description;
    }

    function substractPoints() {
      if (vm.points > 0) {
        vm.points--;
      }
    }

    function addPoints() {
      vm.points++;
    }

    function editActivity() {
      var activity = {
        idActivity: parseInt(currentActivity.idActivity),
        name: vm.activityName,
        points: vm.points,
        startDate: vm.startDate,
        detail: vm.description,
        FK_IDFREQUENCY: parseInt(vm.frequency.idFrequency),
        FK_IDINCHARGE: parseInt(vm.inCharge.id)
      };
      activityManageService.editActivity(activity)
      .then(editActivitySuccess)
      .then($uibModalInstance.close)
      .catch(editActivityError);
    }

    function getFrequencies() {
      activityManageService.getFrequencies()
      .then(function(frequency) {
        vm.frequencies = frequency.data;
      });
    }

    function getMembers() {
      employeeManageService.getEmployeeList()
      .then(function(member) {
        vm.members = member.data;
      });
    }

    function editActivitySuccess() {
      ngNotify.set('Activity Succesfully Updated!', 'success');
    }

    function editActivityError() {
      ngNotify.set('An error ocurred!', 'error');
    }
  }
})();
