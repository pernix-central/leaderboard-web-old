(function() {
  'use.strict';

  angular
    .module('leaderboard.activities')
    .controller('DeleteActivityCtrl', DeleteActivityCtrl);
  /* @ngInject */
  function DeleteActivityCtrl($uibModalInstance,
                              activityManageService,
                              param,
                              ngNotify) {
    var vm = this;
    vm.activity = param;
    vm.deleteActivity = deleteActivity;

    function deleteActivity() {
      activityManageService.deleteActivity(vm.activity)
      .then(deleteActivitySuccess)
      .then($uibModalInstance.close)
      .catch(deleteActivityError);
    }

    function deleteActivitySuccess() {
      ngNotify.set('Activity deleted!', 'success');
    }

    function deleteActivityError() {
      ngNotify.set('An error ocurred!', 'error');
    }
  }  })();
