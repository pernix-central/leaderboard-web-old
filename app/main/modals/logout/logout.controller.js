(function() {
  'use.strict';

  angular
    .module('leaderboard.main')
    .controller('LogoutCtrl', [
      '$uibModalInstance',
      'sessionService',
      LogoutCtrl
    ]);

  /* @ngInject */
  function LogoutCtrl($uibModalInstance,
                      sessionService) {
    var vm = this;

    vm.logoutSession = logoutSession;

    function logoutSession() {
      sessionService.logoutSession().then(function() {
        $uibModalInstance.close();
      });
    }
  }
})();
