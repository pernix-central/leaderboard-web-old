(function() {
  'use.strict';

  angular
    .module('leaderboard.main')
    .controller('AddGroupPointsCtrl', AddGroupPointsCtrl);

  /* @ngInject */
  function AddGroupPointsCtrl() {

    var vm = this;

    vm.activities = [
      'Talleres',
      'Clubs',
      'Encargados',
      'After office',
      'Open Space',
      'Beach Retreat'
    ];

    vm.employees = [
      {
        photo: 'assets/img/gold_person.png',
        name: 'Nombre Apellido Apellido',
        position: 'Software Crafter',
      },
      {
        photo: 'assets/img/silver_person.png',
        name: 'Nombre Apellido Apellido',
        position: 'Software Crafter',
      },
      {
        photo: 'assets/img/bronze_person.png',
        name: 'Nombre Apellido Apellido',
        position: 'Software Crafter',
      },
      {
        photo: 'assets/img/person1.png',
        name: 'Nombre Apellido Apellido',
        position: 'Software Crafter',
      },
      {
        photo: 'assets/img/person3.png',
        name: 'Nombre Apellido Apellido',
        position: 'Software Apprendice',
      },
    ];
  }
})();
