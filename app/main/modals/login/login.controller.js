(function() {
  'use.strict';

  angular
    .module('leaderboard.main')
    .controller('LoginCtrl', [
      '$uibModalInstance',
      'sessionService',
      'modalService',
      '$q',
      LoginCtrl
    ]);

  /* @ngInject */
  function LoginCtrl($uibModalInstance,
                     sessionService,
                     modalService,
                     $q) {

    var vm = this;

    vm.loginSession = loginSession;

    function loginSession() {
      sessionService.loginSession(vm.username, vm.password)
      .then(function() {
        $uibModalInstance.close();
      });
    }
  }
})();
