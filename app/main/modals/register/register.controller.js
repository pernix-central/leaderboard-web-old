(function() {
  'use.strict';

  angular
    .module('leaderboard.main')
    .controller('RegisterCtrl', RegisterCtrl);

  /* @ngInject */
  function RegisterCtrl($uibModalInstance,
                        sessionService,
                        jobManageService,
                        roleManageService,
                        userManageService,
                        employeeManageService,
                        ngNotify) {

    var vm = this;
    vm.jobPositions = [];
    vm.roles = [];
    user = [];
    var userID = null;
    var user = null;
    var member = null;
    this.addMember = addMember;
    activate();

    function activate() {
      loadJobPositions();
      loadRoles();
    }

    function loadJobPositions() {
      jobManageService.getJobPositions()
      .then(function(jobs) {
        vm.jobPositions = jobs.data;
      });
    }

    function loadRoles() {
      roleManageService.getRoles()
      .then(function(roles) {
        vm.roles = roles.data;
      });
    }

    function addMember() {
      var configUser = {
        name: vm.name,
        lastname: vm.lastname,
        idRole: parseInt(vm.role.idRole)
      };
      userManageService.addUser(configUser)
      .then(function(newUser) {
        user = newUser.data;
        userID = user.idUser;
        member = {
          name: vm.name,
          lastname: vm.lastname,
          photoUrl: vm.photoUrl,
          FK_IDUSER: userID,
          FK_IDJOBPOSITION: parseInt(vm.jobPosition.idJobPosition)
        };
        employeeManageService.addMember(member);
      })
      .then(addMemberSuccess)
      .then($uibModalInstance.close)
      .catch(addMemberError);
    }

    function addMemberSuccess() {
      ngNotify.set('Member Succesfully Added!', 'success');
    }

    function addMemberError() {
      ngNotify.set('An error ocurred!', 'error');
    }
  }
})();
