(function() {
  'use.strict';

  angular
    .module('leaderboard.main')
    .controller('AddActivityCtrl', AddActivityCtrl);

  /* @ngInject */
  function AddActivityCtrl($q,
                           $uibModalInstance,
                           activityManageService,
                           employeeManageService,
                           ngNotify) {
    var vm = this;
    vm.substractPoints = substractPoints;
    vm.addPoints = addPoints;
    vm.addActivity = addActivity;
    vm.points = 0;
    activate();

    function activate() {
      getFrequencies();
      getMembers();
    }

    function substractPoints() {
      if (vm.points > 0) {
        vm.points--;
      }
    }

    function addPoints() {
      vm.points++;
    }

    function addActivity() {
      var activity = {
        name: vm.activityName,
        points: vm.points,
        startDate: vm.startDate,
        detail: vm.description,
        FK_IDFREQUENCY: parseInt(vm.frequency.idFrequency),
        FK_IDINCHARGE: parseInt(vm.inCharge.id)
      };
      activityManageService.addActivity(activity)
      .then(addActivitySuccess)
      .then($uibModalInstance.close)
      .catch(addActivityError);
    }

    function getFrequencies() {
      activityManageService.getFrequencies()
      .then(function(frequency) {
        vm.frequencies = frequency.data;
      });
    }

    function getMembers() {
      employeeManageService.getEmployeeList()
      .then(function(member) {
        vm.members = member.data;
      });
    }

    function addActivitySuccess() {
      ngNotify.set('Activity Succesfully Added!', 'success');
    }

    function addActivityError() {
      ngNotify.set('An error ocurred!', 'error');
    }
  }
})();
