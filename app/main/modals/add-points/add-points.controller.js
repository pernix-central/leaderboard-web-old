(function() {
  'use.strict';

  angular
    .module('leaderboard.main')
    .controller('AddPointsCtrl', AddPointsCtrl);

  /* @ngInject */
  function AddPointsCtrl($uibModalInstance,
                         activityManageService,
                         param,
                         addPointsManageService,
                         ngNotify) {

    var vm = this;
    var employee = param;
    vm.addPointsByParticipation = addPointsByParticipation;
    getActivities();

    function addPointsByParticipation() {
      var participation = {
        participationDate: new Date(),
        FK_IDMEMBER: parseInt(param.id),
        FK_IDACTIVITY: parseInt(vm.activity.idActivity)
      };
      addPointsManageService.addPoints(participation)
      .then(addPointsMemberSuccess)
      .then($uibModalInstance.close)
      .catch(addPointsMemberError);
    }

    function getActivities() {
      activityManageService.getActivities()
      .then(function(activities) {
        vm.activities = activities.data;
      });
    }

    function addPointsMemberSuccess() {
      ngNotify.set('Points added!', 'success');
    }

    function addPointsMemberError() {
      ngNotify.set('An error ocurred!', 'error');
    }
  }
})();
