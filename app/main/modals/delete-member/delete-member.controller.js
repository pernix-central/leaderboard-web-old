(function() {
  'use.strict';

  angular
    .module('leaderboard.main')
    .controller('DeleteMemberCtrl', DeleteMemberCtrl);
  /* @ngInject */
  function DeleteMemberCtrl($uibModalInstance,
                            employeeManageService,
                            param,
                            ngNotify) {
    var vm = this;
    vm.employee = param;
    vm.deleteMember = deleteMember;

    function deleteMember() {
      employeeManageService.deleteMember(vm.employee)
      .then(deleteMemberSuccess)
      .then($uibModalInstance.close)
      .catch(deleteMemberError);
    }

    function deleteMemberSuccess() {
      ngNotify.set('Member deleted!', 'success');
    }

    function deleteMemberError() {
      ngNotify.set('An error ocurred!', 'error');
    }
  }  })();
