(function() {
  'use.strict';

  angular
    .module('leaderboard.main')
    .controller('EditEmployeeCtrl', EditEmployeeCtrl);

  /* @ngInject */
  function EditEmployeeCtrl($uibModalInstance,
                            employeeManageService,
                            param,
                            ngNotify,
                            jobManageService) {
    var vm = this;
    var employee = param;
    vm.editMember = editMember;
    activate();

    function activate() {
      vm.employeeName = employee.name;
      vm.photoUrl = employee.photoUrl;
      loadJobPositions();
    }

    function editMember() {
      var member =  {
        idMember: parseInt(employee.id),
        name: vm.employeeName,
        lastname: employee.lastName,
        photoUrl: vm.photoUrl,
        FK_IDUSER: parseInt(employee.id),
        FK_IDJOBPOSITION: parseInt(vm.jobPosition.idJobPosition)
      };
      employeeManageService.editMember(member)
      .then(editMemberSuccess)
      .then($uibModalInstance.close)
      .catch(editMemberError);
    }

    function loadJobPositions() {
      jobManageService.getJobPositions()
      .then(function(jobs) {
        vm.jobPositions = jobs.data;
      });
    }

    function editMemberSuccess() {
      ngNotify.set('Member edited!', 'success');
    }

    function editMemberError() {
      ngNotify.set('An error ocurred!', 'error');
    }
  }
})();
