(function() {
  'use strict';

  angular
    .module('leaderboard.main', ['leaderboard.core'])
    .config(routesConfiguration);

  /* @ngInject */
  function routesConfiguration($stateProvider) {
    $stateProvider
      .state('header.main', {
        cache: false,
        url: '/main',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl as vm'
      });
  }
})();
