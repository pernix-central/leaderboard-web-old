(function() {
  'use strict';

  /* @ngInject */
  angular
    .module('leaderboard.main')
    .constant('MAIN_TEMP_EMPLOYEES', [
      {
        positionRank: 1,
        photo: 'assets/img/gold_person.png',
        name: 'Nombre Apellido Apellido',
        position: 'Software Crafter',
        totalPoints: 40,
        activities: [
          {
            name: 'Talleres',
            points: 2
          },
          {
            name: 'Clubs',
            points: 4
          },
          {
            name: 'Encargados',
            points: 8
          },
          {
            name: 'After office',
            points: 6
          },
          {
            name: 'Open Space',
            points: 8
          },
          {
            name: 'Beach Retreat',
            points: 4
          },
        ]
      },
      {
        positionRank: 2,
        photo: 'assets/img/silver_person.png',
        name: 'Nombre Apellido Apellido',
        position: 'Software Apprendice',
        totalPoints: 39,
        activities: [
          {
            name: 'Talleres',
            points: 2
          },
          {
            name: 'Clubs',
            points: 4
          },
          {
            name: 'Encargados',
            points: 8
          },
          {
            name: 'After office',
            points: 6
          },
          {
            name: 'Open Space',
            points: 8
          },
          {
            name: 'Beach Retreat',
            points: 4
          },
        ]
      },
      {
        positionRank: 3,
        photo: 'assets/img/bronze_person.png',
        name: 'Nombre Apellido Apellido',
        position: 'Software Crafter',
        totalPoints: 33,
        activities: [
          {
            name: 'Talleres',
            points: 2
          },
          {
            name: 'Clubs',
            points: 4
          },
          {
            name: 'Encargados',
            points: 8
          },
          {
            name: 'After office',
            points: 6
          },
          {
            name: 'Open Space',
            points: 8
          },
          {
            name: 'Beach Retreat',
            points: 4
          },
        ]
      },
      {
        positionRank: 4,
        photo: 'assets/img/person1.png',
        name: 'Nombre Apellido Apellido',
        position: 'Designer',
        totalPoints: 32,
        activities: [
          {
            name: 'Talleres',
            points: 2
          },
          {
            name: 'Clubs',
            points: 4
          },
          {
            name: 'Encargados',
            points: 8
          },
          {
            name: 'After office',
            points: 6
          },
          {
            name: 'Open Space',
            points: 8
          },
          {
            name: 'Beach Retreat',
            points: 4
          },
        ]
      },
      {
        positionRank: 5,
        photo: 'assets/img/person3.png',
        name: 'Nombre Apellido Apellido',
        position: 'Software Crafter',
        totalPoints: 31,
        activities: [
          {
            name: 'Talleres',
            points: 2
          },
          {
            name: 'Clubs',
            points: 4
          },
          {
            name: 'Encargados',
            points: 8
          },
          {
            name: 'After office',
            points: 6
          },
          {
            name: 'Open Space',
            points: 8
          },
          {
            name: 'Beach Retreat',
            points: 4
          },
        ]
      }
    ]);
})();
