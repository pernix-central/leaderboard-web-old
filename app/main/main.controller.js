(function() {
  'use.strict';

  angular
    .module('leaderboard.main')
    .controller('MainCtrl', MainCtrl);

  /* @ngInject */
  function MainCtrl(modalService,
                    sessionService,
                    employeeManageService) {

    var vm = this;
    vm.photoSize = photoSize;
    vm.setShowMore = setShowMore;
    vm.showMore = showMore;
    vm.showAddActivity = showAddActivity;
    vm.showAddPoints = showAddPoints;
    vm.showAddGroupPoints = showAddGroupPoints;
    vm.showAddMember = showAddMember;
    vm.editEmployee = showEditEmployee;
    vm.deleteMember = deleteMember;
    vm.sessionActive = sessionService.getSessionActive;
    vm.borderClasses = [
      'table-td-border-gold',
      'table-td-border-silver',
      'table-td-border-bronze'
    ];
    loadData();

    function loadData() {
      vm.employees = [];
      employeeManageService.getEmployeeList()
      .then(function(employeeList) {
        vm.employees = employeeList.data;
        vm.rowState = Array(vm.employees.length).fill(true);
      });
    }

    function showAddPoints(employee) {
      var addPointsTemplate =
      'app/main/modals/add-points/add-points.html';
      modalService.showModalWithParam(
        addPointsTemplate,
        'AddPointsCtrl',
        'md',
        employee
      )
      .closed
      .then(loadData);
    }

    function showAddActivity() {
      var addActivityTemplate =
      'app/main/modals/add-activity/add-activity.html';
      modalService.showModal(addActivityTemplate,
        'AddActivityCtrl',
        'lg'
      );
    }

    function showAddGroupPoints() {
      var addActivityTemplate =
      'app/main/modals/add-group-points/add-group-points.html';
      modalService.showModal(addActivityTemplate,
        'AddGroupPointsCtrl',
        'lg'
      );
    }

    function showAddMember() {
      var registerTemplate =
      'app/main/modals/register/register.html';
      modalService.showModal(registerTemplate,
        'RegisterCtrl',
        'md'
      )
      .closed
      .then(loadData);
    }

    function showEditEmployee(member) {
      var editEmployeeTemplate =
      'app/main/modals/edit-employee/edit-employee.html';
      modalService.showModalWithParam(editEmployeeTemplate,
        'EditEmployeeCtrl',
        'md',
        member
      )
      .closed
      .then(loadData);
    }

    function deleteMember(member) {
      var deleteMemberTemplate =
      'app/main/modals/delete-member/delete-member.html';
      modalService.showModalWithParam(deleteMemberTemplate,
        'DeleteMemberCtrl',
        'md',
        member)
      .closed
      .then(loadData);
    }

    function photoSize(index) {
      return (index == 0) ? 'table-photo-firstPosition' : 'table-photo';
    }

    function setShowMore(index) {
      vm.rowState[index] = !vm.rowState[index];
    }

    function showMore(index) {
      return vm.rowState[index];
    }

    function presentation() {
      $state.go('presentation');
    }
  }
})();
