# Leaderboard

Requirements:
* npm
* Python
* Visual Studio
* MySQL : use ID "root" and password "root" for prevent problems to run.


## Installation
1. `$ git clone git@bitbucket.org:leaderboard-pernix/leaderboard-web.git`
2. `$ git clone git@bitbucket.org:leaderboard-pernix/leaderboard-api.git`
3. `$ cd leaderboard-web`
4. `$ npm install`: clean cache before install

## Scripts
- `$ gulp run`: run development server (watches sass files)
- `$ gulp sass`: builds app.css from sass files
- `$ gulp sass:watch`: watches for changes in sass files and compiles to app.css
- `$ gulp build-index`: fill the index.html

## How to make PRs

Pull Requests should have:

1. Run the `gulp review` command to make sure the there's no styleguide issues.
Attach the result of the command to the PR (A screenshot).

2. The branch name should be based on the Trello task.
![alt text](img/branch_example.png "Branch name example")

In this case the name of the branch would be: `37-fix-styleguide-issues`

3. Commits should have a meaningful name. Only one commit per PR.

4. If the PR involves modifications on a view. Add a screenshot of the view.

## Pull Request Structure

- Description of the Pull Request

- Screenshot of `gulp review` result

- Images (Screenshots)

## Troubbleshoothing

- If the application does not display the data, check the database in �period� and amplify the time range.

## Reviewers

PR will be reviewed by:

- Kevin Escobar
- Johnny Xu
- Matthew Bertelsen
- Jesus Ramos
